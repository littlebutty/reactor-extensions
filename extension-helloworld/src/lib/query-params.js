'use strict';

var getQueryParam = require('get-query-param');

module.exports = function(settings) {
  if (settings.queryParamValue === getQueryParam(settings.queryParam)) {
    return true;
  } else {
    return false;
  }
};
