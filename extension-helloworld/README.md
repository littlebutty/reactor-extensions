# extension-helloworld
[![Build Status][status-image]][status-url] [![Sandbox][sandbox-image]][sandbox-url]

This is a `Hello World` extension for Turbine. It can be used as a starting template for when starting to develop your Turbine extension.

For a list of steps that you must follow in order to create your Turbine extension, you can check the commit list of this project.

## Development Setup
1. Install [node.js](https://nodejs.org/).
2. Clone this repository.
3. After navigating into the project directory, install project dependencies by running `npm install`.
4. Start the sandbox task by running `./node_modules/.bin/reactor-sandbox`.

[status-url]: https://dtm-builder.ut1.mcps.adobe.net/job/extension-helloworld
[status-image]: https://dtm-builder.ut1.mcps.adobe.net/buildStatus/icon?job=extension-helloworld
[sandbox-url]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-helloworld/ws/sandbox/viewSandbox.html
[sandbox-image]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-helloworld/ws/badges/sandbox.svg
