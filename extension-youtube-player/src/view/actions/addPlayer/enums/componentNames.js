const COMPONENT_NAMES = {
  VIDEO_DETAILS: 'videoDetails',
  PLAYER_DIMENSIONS: 'playerDimensions',
  VIDEO_TIMINGS: 'videoTimings'
};

export default COMPONENT_NAMES;
