import React from 'react';
import Select from '@coralui/redux-form-react-coral/lib/Select';
import Checkbox from '@coralui/redux-form-react-coral/lib/Checkbox';
import { Field, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { deepAssign, filterObject } from '../../../utils/formConfigUtils';

const colorPresets = [
  {
    value: 'red',
    label: 'red'
  },
  {
    value: 'white',
    label: 'white'
  }
];

const PlayerParameters = props => (
  <div>
    <Field
      className="u-block u-gapTop"
      name="parameters.autoPlay"
      component={ Checkbox }
    >
      Automatically start the initial video to play when the player loads
    </Field>

    <Field
      className="u-block"
      name="parameters.ccLoadPolicy"
      component={ Checkbox }
    >
      Show by default the closed captions
    </Field>

    <label>
      <span className="Label u-gapRight">Player&quot;s video progress bar color</span>
      <Field
        name="parameters.color"
        component={ Select }
        options={ colorPresets }
      />
    </label>

    <Field
      className="u-block"
      name="parameters.controls"
      component={ Checkbox }
    >
      Display video player controls
    </Field>

    <Field
      className="u-block"
      name="parameters.disableKb"
      component={ Checkbox }
    >
      Disable keyboard commands for this player
    </Field>

    <Field
      className="u-block"
      name="parameters.fs"
      component={ Checkbox }
    >
      Show fullscreen button
    </Field>

    <Field
      className="u-block"
      name="parameters.showVideoAnnotations"
      component={ Checkbox }
    >
      Show video annotations
    </Field>

    <Field
      className="u-block"
      name="parameters.loop"
      component={ Checkbox }
    >
      Enable loop
    </Field>

    <Field
      className="u-block"
      name="parameters.modestBranding"
      disabled={ props.parameters.color === 'white' }
      component={ Checkbox }
    >
      Prevent the YouTube logo from displaying in the control bar
    </Field>

    <Field
      className="u-block"
      name="parameters.rel"
      component={ Checkbox }
    >
      Show related videos when playback of the initial video ends
    </Field>

    <Field
      className="u-block"
      name="parameters.showInfo"
      component={ Checkbox }
    >
      Display the video title and the uploader name before the video starts playing
    </Field>
  </div>
);

export default connect(state => ({
  parameters: {
    color: formValueSelector('default')(state, 'parameters.color')
  }
}))(PlayerParameters);

const fieldNames = [
  'parameters.autoPlay',
  'parameters.ccLoadPolicy',
  'parameters.color',
  'parameters.controls',
  'parameters.disableKb',
  'parameters.fs',
  'parameters.showVideoAnnotations',
  'parameters.loop',
  'parameters.modestBranding',
  'parameters.rel',
  'parameters.showInfo'
];

export const formConfig = {
  settingsToFormValues(values, settings, meta) {
    values = deepAssign({}, values, filterObject(settings, fieldNames));

    return {
      ...values,
      parameters: {
        ...values.parameters,
        color: meta.isNew || !values.parameters.color ? 'red' : values.parameters.color,
        controls: !values.parameters || values.parameters.controls !== false,
        fs: !values.parameters || values.parameters.fs !== false,
        showVideoAnnotations:
          !values.parameters || values.parameters.showVideoAnnotations !== false,
        rel: !values.parameters || values.parameters.rel !== false
      }
    };
  },
  formValuesToSettings(settings, values) {
    settings = deepAssign({}, settings, filterObject(values, fieldNames));

    if (settings.parameters.color === 'white') {
      delete settings.parameters.modestBranding;
    }

    return settings;
  }
};
