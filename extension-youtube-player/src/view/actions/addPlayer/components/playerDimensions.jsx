import React from 'react';
import Textfield from '@coralui/redux-form-react-coral/lib/Textfield';
import { Field } from 'redux-form';
import DecoratedInput from '@reactor/react-components/lib/reduxForm/decoratedInput';
import { deepAssign, filterObject } from '../../../utils/formConfigUtils';
import COMPONENT_NAMES from '../enums/componentNames';
import { isNumber, isSingleDataElementToken } from '../../../utils/validators';

export default () => (
  <div className="ColumnGrid">
    <label className="ColumnGrid-cell">
      <span className="Label u-gapTop">Width</span>
      <div>
        <Field
          name="dimensions.width"
          component={ DecoratedInput }
          inputComponent={ Textfield }
          placeholder="640"
          suffixLabel="px"
          inputClassName="Field--short"
          supportDataElement
        />
      </div>
    </label>
    <label className="ColumnGrid-cell">
      <span className="Label u-gapTop">Height</span>
      <div>
        <Field
          name="dimensions.height"
          component={ DecoratedInput }
          placeholder="360"
          suffixLabel="px"
          inputComponent={ Textfield }
          inputClassName="Field--short"
          supportDataElement
        />
      </div>
    </label>
  </div>
);

const fields = [
  'dimensions.width',
  'dimensions.height'
];

export const formConfig = {
  settingsToFormValues(values, settings) {
    return deepAssign({}, values, filterObject(settings, fields));
  },
  formValuesToSettings(settings, values) {
    return deepAssign({}, settings, filterObject(values, fields));
  },
  validate(errors, values) {
    const {
      width,
      height
    } = values.dimensions || {};

    const playerDimensionsErrors = {};

    const componentsWithErrors = errors.componentsWithErrors ?
      errors.componentsWithErrors.slice() : [];

    if (width &&
      (!isNumber(width) || width < 200) &&
      !isSingleDataElementToken(width)
    ) {
      playerDimensionsErrors.width =
        'Please provide a number that is at least 200 pixels or a data element';
    }

    if (height &&
      (!isNumber(height) || height < 200) &&
      !isSingleDataElementToken(height)
    ) {
      playerDimensionsErrors.height =
        'Please provide a number that is at least 200 pixels or a data element';
    }

    if (Object.keys(playerDimensionsErrors).length) {
      componentsWithErrors.push(COMPONENT_NAMES.PLAYER_DIMENSIONS);

      errors = {
        ...errors,
        dimensions: playerDimensionsErrors
      };
    }

    return {
      ...errors,
      componentsWithErrors
    };
  }
};
