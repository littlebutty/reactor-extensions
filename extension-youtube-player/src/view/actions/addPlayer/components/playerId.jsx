import React from 'react';
import { Field, formValueSelector } from 'redux-form';
import Textfield from '@coralui/redux-form-react-coral/lib/Textfield';
import Select from '@coralui/redux-form-react-coral/lib/Select';
import { connect } from 'react-redux';
import DecoratedInput from '@reactor/react-components/lib/reduxForm/decoratedInput';
import { deepAssign, filterObject, deepSetIfUndefined } from '../../../utils/formConfigUtils';
import COMPONENT_NAMES from '../enums/componentNames';

const PLAYER_ID_TYPES = {
  DEFAULT: 'default',
  CUSTOM: 'custom'
};

const playerIdOptions = [
  {
    value: PLAYER_ID_TYPES.DEFAULT,
    label: 'Automatically generated'
  },
  {
    value: PLAYER_ID_TYPES.CUSTOM,
    label: 'Custom'
  }
];

const PlayerId = props => (
  <div className={ props.className }>
    <label>
      <span className="Label">Player ID:</span>
      <div>
        <Field
          name="playerId.type"
          component={ Select }
          options={ playerIdOptions }
        />
        {
          props.playerId.type === PLAYER_ID_TYPES.CUSTOM ?
            <Field
              name="playerId.value"
              component={ DecoratedInput }
              inputComponent={ Textfield }
              inputClassName="Field--long"
              className="u-gapLeft"
              supportDataElement
            /> : null
        }
      </div>
    </label>
  </div>
);

export default connect(state => ({
  playerId: {
    type: formValueSelector('default')(state, 'playerId.type')
  }
}))(PlayerId);

const fields = [
  'playerId.type',
  'playerId.value'
];

export const formConfig = {
  settingsToFormValues(values, settings) {
    values = deepAssign({}, values, filterObject(settings, fields));
    deepSetIfUndefined(values, 'playerId.type', PLAYER_ID_TYPES.DEFAULT);
    return values;
  },
  formValuesToSettings(settings, values) {
    settings = deepAssign({}, settings, filterObject(values, fields));

    const type = values.playerId.type;

    if (type !== PLAYER_ID_TYPES.CUSTOM) {
      delete settings.playerId.value;
    }

    return settings;
  },
  validate(errors, values) {
    const {
      type,
      value
    } = values.playerId || {};

    const componentsWithErrors = errors.componentsWithErrors ?
      errors.componentsWithErrors.slice() : [];

    if (type === PLAYER_ID_TYPES.CUSTOM && !value) {
      componentsWithErrors.push(COMPONENT_NAMES.VIDEO_DETAILS);

      errors = {
        ...errors,
        playerId: {
          value: 'Please specify a player id'
        }
      };
    }

    return {
      ...errors,
      componentsWithErrors
    };
  }
};
