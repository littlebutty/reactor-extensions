import React from 'react';
import Textfield from '@coralui/redux-form-react-coral/lib/Textfield';
import { Field } from 'redux-form';
import DecoratedInput from '@reactor/react-components/lib/reduxForm/decoratedInput';
import { deepAssign, filterObject } from '../../../utils/formConfigUtils';
import COMPONENT_NAMES from '../enums/componentNames';
import { isNumber, isSingleDataElementToken } from '../../../utils/validators';

export default () => (
  <div>
    <label>
      <span className="Label u-gapRight">Begin playing the video at</span>
      <Field
        name="parameters.start"
        component={ DecoratedInput }
        inputComponent={ Textfield }
        suffixLabel="seconds from the start of the video"
        inputClassName="Field--short"
        supportDataElement
      />
    </label>
    <div className="u-gapTop">
      <label>
        <span className="Label u-gapRight">The player should stop playing the video at</span>
        <Field
          name="parameters.end"
          component={ DecoratedInput }
          suffixLabel="seconds from the start of the video"
          inputComponent={ Textfield }
          inputClassName="Field--short"
          supportDataElement
        />
      </label>
    </div>
  </div>
);

const fields = [
  'parameters.start',
  'parameters.end'
];

export const formConfig = {
  settingsToFormValues(values, settings) {
    return deepAssign({}, values, filterObject(settings, fields));
  },
  formValuesToSettings(settings, values) {
    settings = deepAssign({}, settings, filterObject(values, fields));
    const parameters = settings.parameters || {};

    return {
      ...settings,
      parameters: {
        ...parameters,
        start: isNumber(parameters.start) ? Number(parameters.start) : parameters.start,
        end: isNumber(parameters.end) ? Number(parameters.end) : parameters.end
      }
    };
  },
  validate(errors, values) {
    const {
      start,
      end
    } = values.parameters || {};

    const videoTimingsErrors = {};

    const componentsWithErrors = errors.componentsWithErrors ?
      errors.componentsWithErrors.slice() : [];

    if (start &&
      (!isNumber(start) || start < 0) &&
      !isSingleDataElementToken(start)
    ) {
      videoTimingsErrors.start =
        'Please provide a number higher than 0 or a data element';
    }

    if (end &&
      (!isNumber(end) || end < 0) &&
      !isSingleDataElementToken(end)
    ) {
      videoTimingsErrors.end =
        'Please provide a number higher than 0 or a data element';
    }

    if (Object.keys(videoTimingsErrors).length) {
      componentsWithErrors.push(COMPONENT_NAMES.VIDEO_TIMINGS);

      errors = {
        ...errors,
        parameters: {
          ...errors.parameters,
          ...videoTimingsErrors
        }
      };
    }

    return {
      ...errors,
      componentsWithErrors
    };
  }
};
