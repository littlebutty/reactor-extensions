import React from 'react';
import { Field, formValueSelector } from 'redux-form';
import Textfield from '@coralui/redux-form-react-coral/lib/Textfield';
import Radio from '@coralui/redux-form-react-coral/lib/Radio';
import { connect } from 'react-redux';
import DecoratedInput from '@reactor/react-components/lib/reduxForm/decoratedInput';
import { deepAssign, mergeConfigs, filterObject, deepSetIfUndefined } from '../../../utils/formConfigUtils';
import COMPONENT_NAMES from '../enums/componentNames';
import PlayerId, { formConfig as playerIdFormConfig } from './playerId';

const VIDEO_TYPES = {
  VIDEO: 'video',
  PLAYLIST: 'playlist',
  SEARCH: 'search'
};

const VideoDetails = (props) => {
  const {
    type
  } = props.videoDetails || {};

  return (
    <div>
      <div>
        <Field
          name="videoDetails.type"
          component={ Radio }
          type="radio"
          value={ VIDEO_TYPES.VIDEO }
        >
          I want to play a video
        </Field>
        {
          type === VIDEO_TYPES.VIDEO ? (
            <div>
              <span className="Label u-gapRight">Video ID</span>
              <Field
                name="videoDetails.videoId"
                component={ DecoratedInput }
                inputComponent={ Textfield }
                inputClassName="Field"
                supportDataElement
              />
            </div>
          ) : null
        }
      </div>
      <div>
        <Field
          name="videoDetails.type"
          component={ Radio }
          type="radio"
          value={ VIDEO_TYPES.PLAYLIST }
        >
          I want to play a playlist
        </Field>
        {
          type === VIDEO_TYPES.PLAYLIST ? (
            <div>
              <span className="Label u-gapRight">Playlist ID</span>
              <Field
                name="videoDetails.playlistId"
                component={ DecoratedInput }
                inputComponent={ Textfield }
                inputClassName="Field"
                supportDataElement
              />
            </div>
          ) : null
        }
      </div>
      <div>
        <Field
          name="videoDetails.type"
          component={ Radio }
          type="radio"
          value={ VIDEO_TYPES.SEARCH }
        >
          I want to play the search results for a specified query
        </Field>
        {
          type === VIDEO_TYPES.SEARCH ? (
            <div>
              <span className="Label u-gapRight">Search query</span>
              <Field
                name="videoDetails.query"
                component={ DecoratedInput }
                inputComponent={ Textfield }
                inputClassName="Field"
                supportDataElement
              />
            </div>
          ) : null
        }
      </div>

      <label>
        <span className="Label u-gapRight">
          Append the player to the element matching the CSS selector
        </span>
        <Field
          name="videoDetails.elementSelector"
          component={ DecoratedInput }
          inputComponent={ Textfield }
          supportCssSelector
        />
      </label>

      <PlayerId />
    </div>
  );
};

export default connect(state => ({
  videoDetails: {
    type: formValueSelector('default')(state, 'videoDetails.type')
  }
}))(VideoDetails);

const fields = [
  'videoDetails.type',
  'videoDetails.videoId',
  'videoDetails.playlistId',
  'videoDetails.query',
  'videoDetails.elementSelector'
];

export const formConfig = mergeConfigs(
  playerIdFormConfig,
  {
    settingsToFormValues(values, settings) {
      values = deepAssign({}, values, filterObject(settings, fields));
      deepSetIfUndefined(values, 'videoDetails.type', VIDEO_TYPES.VIDEO);
      return values;
    },
    formValuesToSettings(settings, values) {
      settings = deepAssign({}, settings, filterObject(values, fields));

      const type = values.videoDetails.type;

      if (type === VIDEO_TYPES.VIDEO) {
        delete settings.videoDetails.playlistId;
        delete settings.videoDetails.query;
      } else if (type === VIDEO_TYPES.PLAYLIST) {
        delete settings.videoDetails.videoId;
        delete settings.videoDetails.query;
      } else {
        delete settings.videoDetails.videoId;
        delete settings.videoDetails.playlistId;
      }

      return settings;
    },
    validate(errors, values) {
      const {
        type,
        videoId,
        playlistId,
        query,
        elementSelector
      } = values.videoDetails || {};

      const videoDetailsErrors = {};

      const componentsWithErrors = errors.componentsWithErrors ?
        errors.componentsWithErrors.slice() : [];

      if (type === VIDEO_TYPES.VIDEO) {
        if (!videoId) {
          videoDetailsErrors.videoId = 'Please specify a video id';
        }
      } else if (type === VIDEO_TYPES.PLAYLIST) {
        if (!playlistId) {
          videoDetailsErrors.playlist = 'Please specify a playlist id';
        }
      } else if (!query) {
        videoDetailsErrors.query = 'Please specify a query';
      }

      if (!elementSelector) {
        videoDetailsErrors.elementSelector = 'Please specify a CSS query selector';
      }

      if (Object.keys(videoDetailsErrors).length) {
        componentsWithErrors.push(COMPONENT_NAMES.VIDEO_DETAILS);

        errors = {
          ...errors,
          videoDetails: videoDetailsErrors
        };
      }

      return {
        ...errors,
        componentsWithErrors
      };
    }
  }
);
