import React from 'react';
import Accordion from '@coralui/react-coral/lib/Accordion';
import AccordionItem from '@coralui/react-coral/lib/AccordionItem';
import { mergeConfigs } from '../../utils/formConfigUtils';
import VideoDetails, { formConfig as videoDetailsFormConfig } from './components/videoDetails';
import PlayerDimensions, { formConfig as playerDimensionsFormConfig } from './components/playerDimensions';
import PlayerParameters, { formConfig as playerParametersFormConfig } from './components/playerParameters';
import VideoTimings, { formConfig as videoTimingsFormConfig } from './components/videoTimings';
import eventBus from '../../utils/eventBus';
import COMPONENT_NAMES from './enums/componentNames';

const accordionIndexes = {
  [COMPONENT_NAMES.VIDEO_DETAILS]: 0,
  [COMPONENT_NAMES.PLAYER_DIMENSIONS]: 1,
  [COMPONENT_NAMES.VIDEO_TIMINGS]: 3
};

const onlyUnique = (value, index, self) => self.indexOf(value) === index;

const getIndexesOfComponentsWithErrors = componentsWithErrors =>
  Object.keys(accordionIndexes).map(componentName => (
    componentsWithErrors.indexOf(componentName) !== -1 ? accordionIndexes[componentName] : null
  ));

export default class ExtensionConfiguration extends React.Component {
  constructor() {
    super();

    this.state = {
      selectedIndexes: [0]
    };
  }

  componentDidMount() {
    eventBus.on('validationOccurred', this.validationOccurred, this);
  }

  componentWillUnmount() {
    eventBus.off('validationOccurred', this.validationOccurred);
  }

  onAccordionChange(newSelectedIndexes) {
    if (Array.isArray(newSelectedIndexes)) {
      this.setState({ selectedIndexes: newSelectedIndexes });
    }
  }

  validationOccurred() {
    const { componentsWithErrors } = this.props;
    let { selectedIndexes } = this.state;

    selectedIndexes = selectedIndexes
      .concat(getIndexesOfComponentsWithErrors(componentsWithErrors))
      .filter(onlyUnique);

    this.setState({
      selectedIndexes
    });
  }

  render() {
    const { selectedIndexes } = this.state;

    return (
      <div>
        <Accordion
          multiselectable
          variant="quiet"
          selectedIndex={ selectedIndexes }
          className="Accordion--first"
          onChange={ this.onAccordionChange.bind(this) }
        >
          <AccordionItem header="Video Details">
            <VideoDetails />
          </AccordionItem>
          <AccordionItem header="Player Dimensions">
            <PlayerDimensions />
          </AccordionItem>
          <AccordionItem header="Player Parameters">
            <PlayerParameters />
          </AccordionItem>
          <AccordionItem header="Video Timings">
            <VideoTimings />
          </AccordionItem>
        </Accordion>
      </div>
    );
  }
}

export const formConfig = mergeConfigs(
  videoDetailsFormConfig,
  playerDimensionsFormConfig,
  playerParametersFormConfig,
  videoTimingsFormConfig
);

