import React from 'react';
import Textfield from '@coralui/redux-form-react-coral/lib/Textfield';
import Select from '@coralui/redux-form-react-coral/lib/Select';
import { Field } from 'redux-form';
import DecoratedInput from '@reactor/react-components/lib/reduxForm/decoratedInput';
import { isPositiveNumber } from '../../utils/validators';
import PlayersSelect, { formConfig as playerSelectFormConfig } from '../components/playersSelect';
import { mergeConfigs } from '../../utils/formConfigUtils';

const timePlayedUnit = {
  SECOND: 'second',
  PERCENT: 'percent'
};

const timePlayedUnitOptions = [
  {
    value: timePlayedUnit.SECOND,
    label: 'seconds'
  },
  {
    value: timePlayedUnit.PERCENT,
    label: '%'
  }
];

export default () => (
  <div>
    <PlayersSelect />
    <div className="u-gapTop">
      <label>
        <span className="Label u-gapRight">Trigger when</span>
      </label>
      <Field
        name="amount"
        component={ DecoratedInput }
        inputComponent={ Textfield }
      />
      <Field
        name="unit"
        className="u-gapLeft TimePlayed-unitSelect"
        component={ Select }
        options={ timePlayedUnitOptions }
      />
      <label>
        <span className="Label u-gapLeft">have passed</span>
      </label>
    </div>
  </div>
);

export const formConfig = mergeConfigs(
  playerSelectFormConfig,
  {
    settingsToFormValues: (values, settings) => ({
      ...values,
      ...settings,
      unit: settings.unit || timePlayedUnit.SECOND
    }),
    formValuesToSettings: (settings, values) => ({
      ...settings,
      unit: values.unit,
      amount: Number(values.amount)
    }),
    validate: (errors, values) => {
      errors = {
        ...errors
      };

      if (!isPositiveNumber(values.amount)) {
        errors.amount = 'Please specify a positive number';
      }

      return errors;
    }
  }
);
