import React from 'react';
import Radio from '@coralui/redux-form-react-coral/lib/Radio';
import Textfield from '@coralui/redux-form-react-coral/lib/Textfield';
import { Field, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import DecoratedInput from '@reactor/react-components/lib/reduxForm/decoratedInput';

const PlayerSelect = ({ ...props }) => {
  const { playerSpecificity } = props;

  return (
    <div>
      <div>
        <Field
          name="playerSpecificity"
          component={ Radio }
          type="radio"
          value="any"
        >
          any player
        </Field>
        <Field
          name="playerSpecificity"
          component={ Radio }
          type="radio"
          value="specific"
        >
          specific player
        </Field>
      </div>
      {
        playerSpecificity === 'specific' ?
          <div>
            <span className="Label">Player ID:</span>
            <Field
              name="playerId"
              component={ DecoratedInput }
              inputComponent={ Textfield }
              inputClassName="Field"
              className="u-gapLeft"
              supportDataElement
            />
          </div> : null
      }
    </div>
  );
};

const valueSelector = formValueSelector('default');
const stateToProps = state => ({
  playerSpecificity: valueSelector(state, 'playerSpecificity')
});

export default connect(stateToProps)(PlayerSelect);

export const formConfig = {
  settingsToFormValues: (values, settings, meta) => {
    const { playerId } = settings;

    return {
      ...values,
      playerId,
      playerSpecificity: meta.isNew || !playerId ? 'any' : 'specific'
    };
  },
  formValuesToSettings: (settings, values) => {
    settings = {
      ...settings,
      ...values
    };

    const { playerSpecificity } = values;

    if (playerSpecificity === 'any') {
      delete settings.playerId;
    }

    delete settings.playerSpecificity;

    return settings;
  },
  validate(errors, values) {
    errors = {
      ...errors
    };

    if (values.playerSpecificity !== 'any' && !values.playerId) {
      errors.playerId = 'Please provide a player ID';
    }

    return errors;
  }
};
