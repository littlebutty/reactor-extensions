# extension-youtube-player-iframe-api
[![Build Status][status-image]][status-url] [![Coverage Status][coverage-image]][coverage-url] [![NPM Dependencies][npm-dependencies-image]][npm-dependencies-url] [![Sandbox][sandbox-image]][sandbox-url]

This is the YouTube Player Iframe API extension for Reactor.

## Development Setup
1. Install [node.js](https://nodejs.org/).
1. Clone this repository.
1. After navigating into the project directory, install project dependencies by running `npm install`.
1. See the `scripts` node within package.json for a list of scripts you may run using `npm run`.

See the [extension-support-sandbox README](https://git.corp.adobe.com/reactor/extension-support-sandbox/blob/master/README.md) and the [extension-support-testrunner README](https://git.corp.adobe.com/reactor/extension-support-testrunner/blob/master/README.md) for more information on how to build and test this project.

[status-url]: https://dtm-builder.ut1.mcps.adobe.net/job/extension-youtube-player-iframe-api
[status-image]: https://dtm-builder.ut1.mcps.adobe.net/buildStatus/icon?job=extension-youtube-player-iframe-api
[coverage-url]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-youtube-player-iframe-api/lastStableBuild/cobertura/
[coverage-image]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-youtube-player-iframe-api/ws/badges/coverage.svg
[npm-dependencies-url]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-youtube-player-iframe-api/ws/dependencies.txt
[npm-dependencies-image]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-youtube-player-iframe-api/ws/badges/dependencies.svg
[sandbox-url]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-youtube-player-iframe-api/ws/sandbox/viewSandbox.html
[sandbox-image]: https://dtm-builder.ut1.mcps.adobe.net/view/Reactor-Frontend/job/extension-youtube-player-iframe-api/ws/badges/sandbox.svg
