import renderView from '../renderView';
import ExtensionConfiguration, { formConfig } from './extensionConfiguration';

export default renderView(ExtensionConfiguration, formConfig);
