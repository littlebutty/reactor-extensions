import renderView from '../../renderView';
import SendCustomEvent, { formConfig } from './sendCustomEvent';

export default renderView(SendCustomEvent, formConfig);
