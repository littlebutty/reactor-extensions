import renderView from '../../renderView';
import SendSearchEvent, { formConfig } from './sendSearchEvent';

export default renderView(SendSearchEvent, formConfig);
