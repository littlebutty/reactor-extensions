import renderView from '../../renderView';
import ValueCurrency, { formConfig } from './valueCurrency';

export default renderView(ValueCurrency, formConfig);
